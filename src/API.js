import axios from "axios";

const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER__URL_V1 = SERVER_URL + "api/v1/";

axios.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const logIn = async (email, password) => {
  try {
    await axios.post(SERVER_URL + "login", { email, password }).then((user) => {
      localStorage.setItem("user", JSON.stringify(user));
      localStorage.setItem("token", user.data.data.token);
    });
  } catch (err) {
    throw new Error(err);
  }
};
export const signUp = async (
  firstName,
  lastName,
  email,
  password,
  passwordConfirmation
) => {
  try {
    await axios
      .post(SERVER_URL + "register", {
        firstName,
        lastName,
        email,
        password,
        passwordConfirmation,
      })
      .then((user) => {
        localStorage.setItem("user", JSON.stringify(user));
        localStorage.setItem("token", user.data.data.token);
      });
  } catch (err) {
    throw new Error(err);
  }
};
export const getASingleUser = async (id) => {
  const result = await axios.get(SERVER__URL_V1 + `users/${id}`);
  return result;
};
export const updateUserInfo = async (id, data) => {
  const result = await axios.put(SERVER__URL_V1 + `users/${id}`, data);
  return result.data.data;
};

export const products = async () => {
  const result = await axios.get(SERVER__URL_V1 + "products");
  return result.data.data;
};
export const getASingleProduct = async (id) => {
  const result = await axios.get(SERVER__URL_V1 + `products/${id}`);
  return result.data.data;
};
export const addProduct = async (data) => {
  const result = await axios.post(SERVER__URL_V1 + "products", data);
  return result.data.data;
};
export const editProduct = async (id, data) => {
  const result = await axios.put(SERVER__URL_V1 + `products/${id}`, data);
  return result.data.data;
};
export const deleteProduct = async (id) => {
  await axios.delete(SERVER__URL_V1 + `products/${id}`);
};

export const cart = async () => {
  try {
    const result = await axios.get(SERVER__URL_V1 + "cart");
    return result.data.data;
  } catch {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    window.location.href = "/";
  }
};
export const addToCart = async (productId, qty) => {
  axios.post(SERVER__URL_V1 + "cart/add", {
    productId,
    qty,
  });
};
export const updateCart = async (productId, qty) => {
  axios.post(SERVER__URL_V1 + `cart/update/${productId}`, { qty });
};
export const removeFromCart = async (productId) => {
  axios.post(SERVER__URL_V1 + `cart/remove/${productId}`);
};
export const clearCart = async () => {
  axios.patch(SERVER__URL_V1 + "cart/clear");
};
