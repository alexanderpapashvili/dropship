import "./ProfilePage.css";
import Navigation from "../common/Navigation";
import ProfilePageHeader from "./header/ProfilePageHeader";
import ProfilePageMain from "./main/ProfilePageMain";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";

const ProfilePage = () => {
  const history = useHistory();

  useEffect(() => {
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("user");
    if (token === null || user === null) {
      history.push("/");
    }
  }, []);

  return (
    <>
      <Navigation />
      <div className="profilePage">
        <div className="profileHeaderAndMain"></div>
        <ProfilePageHeader />
        <ProfilePageMain />
      </div>
    </>
  );
};

export default ProfilePage;
