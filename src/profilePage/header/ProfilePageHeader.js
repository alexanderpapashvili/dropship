import "./ProfilePageHeader.css";
import HelpIcon from "../../media/icons/HelpIcon";
import BurgerMenu from "../../common/BurgerMenu";
import SignOutButton from "../../common/buttons/SignOutButton";

const ProfilePageHeader = () => {
  return (
    <div className="profileHeader">
      <div className="profileHeader__title">
        <h2>MY PROFILE</h2>
      </div>
      <div className="profileHeader__burgerMenu">
        <SignOutButton />
        <SignOutButton small />
        <BurgerMenu />
        <HelpIcon />
      </div>
    </div>
  );
};

export default ProfilePageHeader;
