import "./ProfilePageMain.css";
import { Button, makeStyles, TextField } from "@material-ui/core";
import { getASingleUser, updateUserInfo } from "../../API";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { useSnackbar } from "notistack";
import InputAdornment from "@material-ui/core/InputAdornment";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import TitleIcon from "@material-ui/icons/Title";
import VpnKeyIcon from "@material-ui/icons/VpnKey";

const validationSchema = yup.object().shape({
  firstName: yup.string(),
  lastName: yup.string(),
  email: yup.string().email(),
  password: yup
    .string()
    .min(5, "Password is too short")
    .max(20, "Password is too long"),
});

const useStyles = makeStyles(() => ({
  inputField: {
    width: "85%",
    height: "47px",
    color: "#61d4df",
    marginTop: "35px",
    backgroundColor: "white",
  },
  updateButton: {
    color: "white",
    fontFamily: "'Poppins', sans-serif",
    fontSize: "16px",
    fontWeight: "600",
    backgroundColor: "#61d4df",
    width: "30%",
    height: "44px",
    textTransform: "capitalize",
    margin: "20px 70px 20px 0",
    cursor: "pointer",
    alignSelf: "flex-end",
  },
}));

const ProfilePageMain = () => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const performUpdate = (values) => {
    updateUserInfo(
      JSON.parse(localStorage.getItem("user")).data.data.id,
      values
    )
      .then((result) => {
        enqueueSnackbar("Info successfully updated!", {
          variant: "success",
        });
        getASingleUser(
          JSON.parse(localStorage.getItem("user")).data.data.id
        ).then((res) => {
          localStorage.setItem("user", JSON.stringify(res));
        });
      })
      .catch((err) => {
        enqueueSnackbar("Something went wrong. Please try again!", {
          variant: "error",
        });
      });
  };

  const costumeInputFirstName = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.firstName && errors.firstName ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="First Name"
        id="input-with-icon-textField"
        placeholder="John"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <TitleIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputLastName = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.lastName && errors.lastName ? true : false}
        className={classes.inputField}
        placeholder="Doe"
        variant="outlined"
        label="Last Name"
        id="input-with-icon-textField"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <TitleIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputEmail = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.email && errors.email ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="Email"
        id="input-with-icon-textField"
        placeholder="johndoe@example.com"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <MailOutlineIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputPassword = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.password && errors.password ? true : false}
        className={classes.inputField}
        placeholder="••••••••••"
        type="password"
        variant="outlined"
        label="Password"
        id="input-with-icon-textField"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKeyIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  return (
    <div className="profilePageMain">
      <div className="profilePageMain__form--wrapper">
        <Formik
          enableReinitialize
          initialValues={{
            firstName:
              JSON.parse(localStorage.getItem("user")) &&
              JSON.parse(localStorage.getItem("user")).data.data.firstName,
            lastName:
              JSON.parse(localStorage.getItem("user")) &&
              JSON.parse(localStorage.getItem("user")).data.data.lastName,
            email:
              JSON.parse(localStorage.getItem("user")) &&
              JSON.parse(localStorage.getItem("user")).data.data.email,
            password: "",
          }}
          onSubmit={performUpdate}
          validationSchema={validationSchema}
        >
          <Form className="infoUpdate__form">
            <div className="infoUpdate__form--wrapper">
              <div className="userInfoUpdate">
                <h3>Update Personal Information</h3>
                <div className="changePersonalInfo">
                  <Field name="firstName" component={costumeInputFirstName} />
                  <ErrorMessage
                    className="errorMessage"
                    name="firstName"
                    component="div"
                  />
                  <Field name="lastName" component={costumeInputLastName} />
                  <ErrorMessage
                    className="errorMessage"
                    name="lastName"
                    component="div"
                  />
                </div>
                <h3>Update Contact Information</h3>
                <div className="changeContactInfo">
                  <Field name="email" component={costumeInputEmail} />
                  <ErrorMessage
                    className="errorMessage"
                    name="email"
                    component="div"
                  />
                </div>
              </div>
              <div className="userPasswordUpdate">
                <h3>Update Password</h3>
                <div className="changePasswordInfo">
                  <Field name="password" component={costumeInputPassword} />
                  <ErrorMessage
                    className="errorMessage"
                    name="password"
                    component="div"
                  />
                </div>
              </div>
            </div>
            <Button
              variant="contained"
              className={classes.updateButton}
              type="submit"
            >
              update
            </Button>
          </Form>
        </Formik>
      </div>
    </div>
  );
};

export default ProfilePageMain;
