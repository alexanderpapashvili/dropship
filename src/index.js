import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider, createMuiTheme } from "@material-ui/core";
import { Provider } from "react-redux";
import { combineReducers, createStore } from "redux";
import UIReducers from "./reducers/UIReducers/UIReducers";
import CommonReducers from "./reducers/commonReducers/commonReducers";
import CatalogPageReducers from "./reducers/catalogPageReducers/catalogPageReducers";
import CartPageReducers from "./reducers/cartPageReducers/CartPageReducers";
import { SnackbarProvider } from "notistack";

const mainTheme = createMuiTheme({
  breakpoints: {
    values: {
      sm: 650,
      md: 1350,
      lg: 1600,
    },
  },
  palette: {
    primary: {
      main: "#61d5df",
    },
    secondary: {
      main: "#30387b",
    },
  },
});

const reducers = combineReducers({
  UI: UIReducers,
  common: CommonReducers,
  catalogPage: CatalogPageReducers,
  cartPage: CartPageReducers,
});

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <React.StrictMode>
    <SnackbarProvider maxSnack={5}>
      <Provider store={store}>
        <BrowserRouter>
          <ThemeProvider theme={mainTheme}>
            <App />
          </ThemeProvider>
        </BrowserRouter>
      </Provider>
    </SnackbarProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
