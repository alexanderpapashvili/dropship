import "./App.css";
import { Switch, Route } from "react-router-dom";
import ProfilePage from "./profilePage/ProfilePage";
import CatalogPage from "./catalogPage/CatalogPage";
import CartPage from "./cartPage/CartPage";
import ComingSoon from "./comingSoonPage/ComingSoon";
import LogIn from "./landingPage/logIn/LogIn";
import SignUp from "./landingPage/signUp/SignUp";
import LandingPage from "./landingPage/LandingPage";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/profile">
          <ProfilePage />
        </Route>
        <Route path="/dashboard">
          <ComingSoon />
        </Route>
        <Route path="/catalog/:productId?">
          <CatalogPage />
        </Route>
        <Route path="/inventory">
          <ComingSoon />
        </Route>
        <Route path="/cart">
          <CartPage />
        </Route>
        <Route path="/orders">
          <ComingSoon />
        </Route>
        <Route path="/transactions">
          <ComingSoon />
        </Route>
        <Route path="/storesList">
          <ComingSoon />
        </Route>
        <Route path="/logIn">
          <LogIn />
        </Route>
        <Route path="/signUp">
          <SignUp />
        </Route>
        <Route path="/">
          <LandingPage />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
