import "./ComingSoon.css";
import { useEffect } from "react";
import ASAP from "../media/asap.png";
import Navigation from "../common/Navigation";
import { useHistory } from "react-router-dom";

const ComingSoon = () => {
  const history = useHistory();

  useEffect(() => {
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("user");
    if (token === null || user === null) {
      history.push("/");
    }
  }, []);

  return (
    <>
      <Navigation />
      <div className="comingSoon">
        <div className="comingSoon__icon">
          <img src={ASAP} alt="Coming Soon" />
        </div>
      </div>
    </>
  );
};

export default ComingSoon;
