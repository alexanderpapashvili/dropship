import "./HelpIcon.css";
import helpIcon from "../helpIcon.png";

const HelpIcon = () => {
  return <img src={helpIcon} className="helpIcon" alt="Help" />;
};

export default HelpIcon;
