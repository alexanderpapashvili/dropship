import "./CartItems.css";
import LoadingGif from "../../common/LoadingGif";
import emptyCartBig from "../../media/emptyCartBig.png";
import CartItemsTable from "../cartItems/CartItemsTable";
import { useSelector } from "react-redux";

const CartItems = () => {
  const loadingGifCart = useSelector((state) => state.UI.loadingGifCart);
  const cartProducts = useSelector((store) => store.cartPage.cartProductsList);

  return (
    <div className="cartItems__wrapper">
      <LoadingGif cart />
      <section
        className={`cartItems ${loadingGifCart ? " cartItems--hide" : ""} ${
          cartProducts.cartItem && cartProducts.cartItem.items.length === 0
            ? " cartItems--hide"
            : ""
        }`}
      >
        <table>
          <tbody>
            <tr>
              <th className="table__header--image">Image</th>
              <th className="table__header--title">Title</th>
              <th className="table__header--price">Price</th>
              <th className="table__header--quantity">Quantity</th>
              <th className="table__header--button"></th>
            </tr>
            {cartProducts.cartItem &&
              cartProducts.cartItem.items.map((item) => {
                return (
                  <CartItemsTable
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    price={item.price}
                    image={item.image}
                    qty={item.qty}
                  />
                );
              })}
          </tbody>
        </table>
      </section>
      <div
        className={`emptyCartIcon ${
          cartProducts.cartItem && cartProducts.cartItem.items.length === 0
            ? " emptyCartIcon--show"
            : ""
        }`}
      >
        <img src={emptyCartBig} alt="empty cart" />
      </div>
    </div>
  );
};

export default CartItems;
