import "./CartItemsTable.css";
import RemoveFromCartButton from "../../common/buttons/RemoveFromCartButton";
import plusIcon from "../../media/plus.png";
import minusIcon from "../../media/minus.png";
import { updateCart, removeFromCart, cart as cartAPI } from "../../API";
import { useState } from "react";
import { useSnackbar } from "notistack";
import { useDispatch } from "react-redux";
import { cartItemsRequest } from "../../reducers/cartPageReducers/cartPageActions";

const CartItemsTable = ({ id, title, price, image, qty }) => {
  const dispatch = useDispatch();
  const [itemQTY, setItemQTY] = useState(qty);
  const { enqueueSnackbar } = useSnackbar();

  const plus = () => {
    setItemQTY(itemQTY + 1);
    updateCart(id, itemQTY + 1).then(() => {
      enqueueSnackbar("Quantity raised successfully", { variant: "success" });
      setTimeout(() => {
        cartAPI()
          .then((result) => {
            dispatch(cartItemsRequest(result));
          })
          .catch((err) => {
            enqueueSnackbar({ err }, { variant: "error" });
          });
      }, 200);
    });
  };

  const minus = () => {
    if (itemQTY > 1) {
      setItemQTY(itemQTY - 1);
      updateCart(id, itemQTY - 1).then(() => {
        enqueueSnackbar("Quantity reduced successfully", {
          variant: "success",
        });
        setTimeout(() => {
          cartAPI()
            .then((result) => {
              dispatch(cartItemsRequest(result));
            })
            .catch((err) => {
              enqueueSnackbar({ err }, { variant: "error" });
            });
        }, 200);
      });
    } else {
      setItemQTY(itemQTY - 1);
      removeFromCart(id).then(() => {
        enqueueSnackbar("Item removed from cart", { variant: "success" });
        setTimeout(() => {
          cartAPI()
            .then((result) => {
              dispatch(cartItemsRequest(result));
            })
            .catch((err) => {
              enqueueSnackbar({ err }, { variant: "error" });
            });
        }, 100);
      });
    }
  };

  return (
    <tr className="row">
      <td className="row__image">
        <img src={image} alt="Product Image" />
      </td>
      <td>{title}</td>
      <td>{price}</td>
      <td className="row__quantity--td">
        <div className="row__quantity--wrapper">
          <div
            className="row__qty--minus"
            onClick={() => {
              minus();
            }}
          >
            <img src={minusIcon} alt="minus" />
          </div>
          <div>{itemQTY}</div>
          <div
            className="row__qty--plus"
            onClick={() => {
              plus();
            }}
          >
            <img src={plusIcon} alt="plus" />
          </div>
        </div>
      </td>
      <td>
        <RemoveFromCartButton itemId={id} />
      </td>
    </tr>
  );
};

export default CartItemsTable;
