import "./CartPage.css";
import Navigation from "../common/Navigation";
import CartHeader from "./header/CartHeader";
import CartItems from "./cartItems/CartItems";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { cart as cartAPI } from "../API";
import { useDispatch } from "react-redux";
import { useSnackbar } from "notistack";
import { cartItemsRequest } from "../reducers/cartPageReducers/cartPageActions";
import { cartLoadingGif } from "../reducers/UIReducers/UIReducersActions";

const Inventory = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("user");
    if (token === null || user === null) {
      history.push("/");
    }
  }, []);

  useEffect(() => {
    dispatch(cartLoadingGif(true));
    cartAPI()
      .then((result) => {
        dispatch(cartItemsRequest(result));
      })
      .catch((err) => {
        enqueueSnackbar(`${err}`, { variant: "error" });
      })
      .finally(() => {
        dispatch(cartLoadingGif(false));
      });
  }, []);

  return (
    <>
      <Navigation />
      <div className="cartHeaderAndMain">
        <CartHeader />
        <CartItems />
      </div>
    </>
  );
};

export default Inventory;
