import "./CartQTY.css";
import { useSelector } from "react-redux";

const CartQTY = () => {
  const cartProducts = useSelector((store) => store.cartPage.cartProductsList);
  return (
    <div className="cartQTY">
      <span className="cartQTY--large">
        SHOPPING CART : &nbsp;
        {cartProducts.cartItem && cartProducts.cartItem.items.length}
      </span>
      <span className="cartQTY--small">
        {cartProducts.cartItem && cartProducts.cartItem.items.length} &nbsp;
        Items
      </span>
    </div>
  );
};

export default CartQTY;
