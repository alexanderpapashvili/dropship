import "./CartInfo.css";
import { useSelector } from "react-redux";
import BackToShoppingButton from "../../common/buttons/BackToShoppingButton";
import CheckoutButton from "../../common/buttons/CheckoutButton";

const CartInfo = () => {
  const cartProducts = useSelector((store) => store.cartPage.cartProductsList);

  return (
    <div className="cartInfo">
      <div className="cartInfo__details">
        <div>
          <BackToShoppingButton />
        </div>
        <div className="cartInfo__total--wrapper">
          <span className="cartInfo__orderTotal">
            ORDER TOTAL : &nbsp;
            {cartProducts.cartItem && cartProducts.cartItem.totalAmount}
          </span>
          <CheckoutButton />
        </div>
      </div>
    </div>
  );
};

export default CartInfo;
