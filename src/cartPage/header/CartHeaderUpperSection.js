import "./CartHeaderUpperSection.css";
import EmptyCartButton from "../../common/buttons/EmptyCartButton";
import CartQTY from "./CartQTY";
import HelpIcon from "../../media/icons/HelpIcon";
import BurgerMenu from "../../common/BurgerMenu";

const CartHeaderUpperSection = () => {
  return (
    <div className="cartHeaderUpperSection">
      <div className="cartHeader__emptyCart">
        <EmptyCartButton />
        <EmptyCartButton small />
        <CartQTY />
      </div>
      <div className="cartHeader__burgerMenu">
        <BurgerMenu />
        <HelpIcon />
      </div>
    </div>
  );
};

export default CartHeaderUpperSection;
