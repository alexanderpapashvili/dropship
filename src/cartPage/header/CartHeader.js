import "./CartHeader.css";
import CartHeaderUpperSection from "./CartHeaderUpperSection";
import CartInfo from "./CartInfo";

const CartHeader = () => {
  return (
    <div className="cartHeader">
      <CartHeaderUpperSection />
      <CartInfo />
    </div>
  );
};

export default CartHeader;
