import "./SidebarBackButton.css";
import Arrow from "../../media/arrowLeft.png";
import { useDispatch, useSelector } from "react-redux";
import { openFullSizeSidebar as openFullSizeSidebarAction } from "../../reducers/UIReducers/UIReducersActions";

const SidebarBackButton = () => {
  const dispatch = useDispatch();
  const fullSizeSidebar = useSelector((state) => state.UI.openFullSizeSidebar);

  const close = () => {
    dispatch(openFullSizeSidebarAction(false));
  };

  return (
    <button
      className={`sidebarBackButton ${
        fullSizeSidebar ? " sidebarBackButton--show" : ""
      }`}
      onClick={close}
    >
      <img src={Arrow} alt="" />
      <span>Back</span>
    </button>
  );
};

export default SidebarBackButton;
