import "./ResetFilterButton.css";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { resetingFilters } from "../../reducers/catalogPageReducers/catalogPageActions";
import { searchQuery } from "../../reducers/catalogPageReducers/catalogPageActions";
import { searchError } from "../../reducers/UIReducers/UIReducersActions";

const ResetFilterButton = ({
  dropDownFirst,
  dropDownSecond,
  setDropDownFirst,
  setDropDownSecond,
  valueOne,
  setValueOne,
  valueTwo,
  setValueTwo,
}) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const fullSizeSidebar = useSelector((state) => state.UI.openFullSizeSidebar);

  const reset = () => {
    if (valueOne !== [0, 1000] || valueTwo !== [3, 98]) {
      setDropDownFirst(false);
      setDropDownSecond(false);
      dispatch(searchQuery(""));
      dispatch(searchError(false));
      setValueOne([0, 1000]);
      setValueTwo([3, 98]);
      history.push("/catalog");
      dispatch(resetingFilters(JSON.parse(localStorage.getItem("products"))));
    } else if (dropDownFirst === true || dropDownSecond === true) {
      setDropDownFirst(false);
      setDropDownSecond(false);
    }
  };

  return (
    <button
      className={`resetFilterButton ${
        fullSizeSidebar ? " resetFilterButtonScreen" : ""
      }`}
      onClick={reset}
    >
      Reset Filter
    </button>
  );
};

export default ResetFilterButton;
