import "./SelectAllButton.css";
import { useDispatch } from "react-redux";
import { selectAllItems } from "../../reducers/catalogPageReducers/catalogPageActions";
import { selectedItemsArray as selectedItemsArrayAction } from "../../reducers/catalogPageReducers/catalogPageActions";
import { useSelector } from "react-redux";

const SelectAllButton = () => {
  const dispatch = useDispatch();
  const productList = useSelector(
    (state) => state.catalogPage.catalogProductsList
  );

  const change = () => {
    setTimeout(() => {
      dispatch(selectAllItems(false));
      dispatch(selectAllItems(true));
      dispatch(selectedItemsArrayAction(productList));
    }, 1);
  };

  return (
    <button className="selectAllButton" onClick={change}>
      select all
    </button>
  );
};

export default SelectAllButton;
