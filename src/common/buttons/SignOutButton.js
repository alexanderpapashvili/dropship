import "./SignOutButton.css";
import logOutIcon from "../../media/logoutIcon.png";

const SignOutButton = ({ small }) => {
  const logOut = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    window.location.href = "/";
  };
  const image = () => {
    return (
      <img className="logOutButton__icon" src={logOutIcon} alt="Sign Out" />
    );
  };
  return (
    <button
      className={`logOutButton ${
        small ? " logOutButton--small" : " logOutButton--large"
      }`}
      onClick={logOut}
    >
      {small ? image() : "Sign out"}
    </button>
  );
};

export default SignOutButton;
