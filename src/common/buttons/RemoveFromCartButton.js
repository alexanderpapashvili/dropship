import "./RemoveFromCartButton.css";
import removeFromCartIcon from "../../media/removeFromCart.svg";
import { removeFromCart, cart as cartAPI } from "../../API";
import { useSnackbar } from "notistack";
import { useDispatch } from "react-redux";
import { cartItemsRequest } from "../../reducers/cartPageReducers/cartPageActions";

const RemoveFromCartButton = ({ itemId }) => {
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  return (
    <button
      className="removeFromCartButton"
      onClick={() => {
        removeFromCart(itemId).then(() => {
          enqueueSnackbar("Item removed from cart", { variant: "success" });
          setTimeout(() => {
            cartAPI()
              .then((result) => {
                dispatch(cartItemsRequest(result));
              })
              .catch((err) => {
                enqueueSnackbar({ err }, { variant: "error" });
              });
          }, 200);
        });
      }}
    >
      <img src={removeFromCartIcon} alt="Remove From Cart" />
    </button>
  );
};
export default RemoveFromCartButton;
