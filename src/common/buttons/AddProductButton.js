import "./AddProductButton.css";
import { useDispatch } from "react-redux";
import { openProductMod } from "../../reducers/UIReducers/UIReducersActions";

const AddProductButton = () => {
  const dispatch = useDispatch();

  const add = () => {
    dispatch(openProductMod(true));
  };

  return (
    <button
      className={`addProductButton ${
        JSON.parse(localStorage.getItem("user")).data.data.isAdmin
          ? " addProductButton--show"
          : ""
      }`}
      onClick={add}
    >
      Add product
    </button>
  );
};

export default AddProductButton;
