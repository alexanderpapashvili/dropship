import "./AddToCartButtonSmall.css";
import shoppingCartWhite from "../../media/shoppingCartWhite.svg";
import { addToCart } from "../../API";
import { useSelector } from "react-redux";
import { useSnackbar } from "notistack";

const AddToCartButtonSmall = ({ showSearch }) => {
  const selectedItems = useSelector((store) => store.catalogPage.selectedItems);
  const { enqueueSnackbar } = useSnackbar();

  const add = () => {
    let idArray = [];
    selectedItems.map((e) => idArray.push(e.id));
    idArray.reduce(
      (chain, id) => chain.then(() => addToCart(id, 1)),
      Promise.resolve()
    );
    enqueueSnackbar("Products added to cart successfully!", {
      variant: "success",
    });
  };

  return (
    <button
      onClick={add}
      className={`addToCartButtonSmall ${
        showSearch ? " addToCartButtonSmall--hide" : ""
      }`}
    >
      <img src={shoppingCartWhite} alt="add to cart" />
    </button>
  );
};

export default AddToCartButtonSmall;
