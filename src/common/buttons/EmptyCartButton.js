import "./EmptyCartButton.css";
import emptyCartIcon from "../../media/emptyCart.png";
import { clearCart } from "../../API";
import { useSnackbar } from "notistack";
import { cart as cartAPI } from "../../API";
import { useDispatch } from "react-redux";
import { cartItemsRequest } from "../../reducers/cartPageReducers/cartPageActions";
import { cartLoadingGif } from "../../reducers/UIReducers/UIReducersActions";

const EmptyCartButton = ({ small }) => {
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const image = () => {
    return (
      <img
        className="emptyCartButton__icon"
        src={emptyCartIcon}
        alt="EmptyCart"
      />
    );
  };

  return (
    <button
      className={`emptyCartButton ${
        small ? " emptyCartButton--small" : " emptyCartButton--large"
      }`}
      onClick={() => {
        clearCart().then(() => {
          enqueueSnackbar("Cart is empty", { variant: "success" });
          dispatch(cartLoadingGif(true));
          setTimeout(() => {
            cartAPI()
              .then((result) => {
                dispatch(cartItemsRequest(result));
              })
              .catch((err) => {
                enqueueSnackbar({ err }, { variant: "error" });
              })
              .finally(() => {
                dispatch(cartLoadingGif(false));
              });
          }, 300);
        });
      }}
    >
      {small ? image() : "Empty Cart"}
    </button>
  );
};

export default EmptyCartButton;
