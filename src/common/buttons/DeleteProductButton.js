import "./DeleteProductButton.css";
import deleteProductIcon from "../../media/delete.svg";
import { deleteProduct as deleteProductAPI, products } from "../../API";
import { useSnackbar } from "notistack";
import { useDispatch } from "react-redux";
import { catalogItemsRequest } from "../../reducers/catalogPageReducers/catalogPageActions";

const DeleteProductButton = ({ id }) => {
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const deleteProduct = (e) => {
    e.stopPropagation();
    deleteProductAPI(id).then(() => {
      products().then((result) => {
        dispatch(catalogItemsRequest(result));
      });
      enqueueSnackbar("Product deleted successfully!", { variant: "success" });
    });
  };

  return (
    <button
      className={`deleteProductButton ${
        JSON.parse(localStorage.getItem("user")).data.data.isAdmin
          ? " deleteProductButton--show"
          : ""
      }`}
      onClick={deleteProduct}
    >
      <img src={deleteProductIcon} alt="Delete Product" />
    </button>
  );
};

export default DeleteProductButton;
