import "./CheckoutButton.css";

const CheckoutButton = () => {
  return <button className="checkoutButton">Checkout</button>;
};

export default CheckoutButton;
