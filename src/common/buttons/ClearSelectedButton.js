import "./ClearSelectedButton.css";
import { useDispatch, useSelector } from "react-redux";
import { selectAllItems } from "../../reducers/catalogPageReducers/catalogPageActions";
import { selectSingleItem } from "../../reducers/catalogPageReducers/catalogPageActions";
import { selectedItemsArray } from "../../reducers/catalogPageReducers/catalogPageActions";

const ClearSelectedButton = () => {
  const dispatch = useDispatch();

  const selectedItems = useSelector((store) => store.catalogPage.selectedItems);

  const clear = () => {
    dispatch(selectAllItems(false));
    dispatch(selectSingleItem(false));
    dispatch(selectedItemsArray([]));
  };

  return (
    <button
      className={`clearSelectedButton ${
        selectedItems.length > 0 ? " clearSelectedButton--show" : ""
      }`}
      onClick={clear}
    >
      Clear Selected
    </button>
  );
};

export default ClearSelectedButton;
