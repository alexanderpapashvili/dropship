import "./FilterButton.css";
import filterIcon from "../../media/filterIcon.png";
import { useDispatch } from "react-redux";
import { openFullSizeSidebar } from "../../reducers/UIReducers/UIReducersActions";

const FilterButton = ({ small }) => {
  const dispatch = useDispatch();

  const open = () => {
    dispatch(openFullSizeSidebar(true));
  };
  const image = () => {
    return (
      <img className="filterButton__icon" src={filterIcon} alt="filterIcon" />
    );
  };
  return (
    <button
      className={`filterButton ${
        small ? " filterButton--small" : " filterButton--large"
      }`}
      onClick={open}
    >
      {small ? image() : "Filter"}
    </button>
  );
};

export default FilterButton;
