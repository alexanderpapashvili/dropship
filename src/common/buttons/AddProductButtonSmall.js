import "./AddProductButtonSmall.css";
import addProduct from "../../media/addProduct.svg";
import { useDispatch } from "react-redux";
import { openProductMod } from "../../reducers/UIReducers/UIReducersActions";

const AddProductButtonSmall = ({ showSearch }) => {
  const dispatch = useDispatch();

  const add = () => {
    dispatch(openProductMod(true));
  };

  return (
    <button
      className={`addProductButtonSmall ${
        showSearch ? " addProductButtonSmall--hide" : ""
      } ${
        JSON.parse(localStorage.getItem("user")).data.data.isAdmin
          ? " addProductButtonSmall--show"
          : ""
      }`}
      onClick={add}
    >
      <img src={addProduct} alt="add to cart" />
    </button>
  );
};

export default AddProductButtonSmall;
