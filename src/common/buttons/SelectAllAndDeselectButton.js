import "./SelectAllAndDeselectButton.css";
import Tick from "../../media/tickForSelectAllAndDiselectButton.png";
import { useDispatch, useSelector } from "react-redux";
import { selectAllItems } from "../../reducers/catalogPageReducers/catalogPageActions";
import { selectSingleItem as selectSingleItemAction } from "../../reducers/catalogPageReducers/catalogPageActions";
import { selectedItemsArray } from "../../reducers/catalogPageReducers/catalogPageActions";

const SelectAllAndDeselectButton = () => {
  const dispatch = useDispatch();

  const selectAll = useSelector((store) => store.catalogPage.selectAll);
  const selectSingleItem = useSelector(
    (store) => store.catalogPage.selectSingleItem
  );
  const productList = useSelector(
    (state) => state.catalogPage.catalogProductsList
  );

  const change = () => {
    if (selectAll === true || selectSingleItem === true) {
      dispatch(selectAllItems(false));
      dispatch(selectSingleItemAction(false));
      dispatch(selectedItemsArray([]));
    } else {
      dispatch(selectAllItems(true));
      dispatch(selectedItemsArray(productList));
    }
  };

  return (
    <button className="selectAllAndDeselectButton" onClick={change}>
      <img src={Tick} alt="" />
    </button>
  );
};

export default SelectAllAndDeselectButton;
