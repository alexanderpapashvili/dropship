import "./AddToCartButton.css";
import { addToCart } from "../../API";
import { useSelector } from "react-redux";
import { useSnackbar } from "notistack";

const AddToCartButton = ({ itemHover, modal, itemId, cartPage }) => {
  const selectedItems = useSelector((store) => store.catalogPage.selectedItems);
  const { enqueueSnackbar } = useSnackbar();

  const addToCartALL = async () => {
    let idArray = [];
    selectedItems.map((e) => idArray.push(e.id));
    idArray.reduce(
      (chain, id) => chain.then(() => addToCart(id, 1)),
      Promise.resolve()
    );
    enqueueSnackbar("Products added to cart successfully!", {
      variant: "success",
    });
  };

  const handleAddToCart = (e) => {
    if (itemHover === true) {
      e.stopPropagation();
      addToCart(itemId, 1)
        .then(() => {
          enqueueSnackbar("Product added to cart successfully!", {
            variant: "success",
          });
        })
        .catch((err) => {
          enqueueSnackbar({ err }, { variant: "error" });
        });
    } else {
      addToCartALL();
    }
  };

  return (
    <button
      className={`addToCartButton ${
        itemHover ? " addToCartButton--itemHover" : ""
      } ${modal ? " addToCartButton--modal" : ""} ${
        cartPage ? " addToCartButton--hide " : ""
      }`}
      onClick={handleAddToCart}
    >
      Add To Cart
    </button>
  );
};

export default AddToCartButton;
