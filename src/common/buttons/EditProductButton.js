import "./EditProductButton.css";
import editProductIcon from "../../media/edit.svg";
import { useDispatch } from "react-redux";
import { openProductMod } from "../../reducers/UIReducers/UIReducersActions";

const EditProductButton = ({ id }) => {
  const dispatch = useDispatch();

  const editProduct = (e) => {
    e.stopPropagation();
    dispatch(openProductMod(id));
  };

  return (
    <button
      className={`editProductButton ${
        JSON.parse(localStorage.getItem("user")).data.data.isAdmin
          ? " editProductButton--show"
          : ""
      }`}
      onClick={editProduct}
    >
      <img src={editProductIcon} alt="Edit Product" />
    </button>
  );
};

export default EditProductButton;
