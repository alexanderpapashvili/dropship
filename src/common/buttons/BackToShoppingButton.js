import "./BackToShoppingButton.css";

const BackToShoppingButton = () => {
  const backToShopping = () => {
    window.location.href = "/catalog";
  };
  return (
    <button className="backToShoppingButton" onClick={backToShopping}>
      Back to shopping
    </button>
  );
};

export default BackToShoppingButton;
