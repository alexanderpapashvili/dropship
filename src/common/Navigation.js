import "./Navigation.css";
import { Link } from "react-router-dom";
import logo from "../media/Logo.png";
import profile from "../media/user.png";
import dashboard from "../media/speed.png";
import catalog from "../media/list.png";
import inventory from "../media/cube.png";
import cart from "../media/shoppingCart.png";
import orders from "../media/clipboardChecked.png";
import transactions from "../media/oppositArrows.png";
import storesList from "../media/clipboard.png";
import dashboardColor from "../media/speedColor.png";
import catalogColor from "../media/listColor.png";
import inventoryColor from "../media/cubeColor.png";
import cartColor from "../media/shoppingCartColor.png";
import ordersColor from "../media/clipboardCheckedColor.png";
import transactionsColor from "../media/oppositArrowsColor.png";
import storesListColor from "../media/clipboardColor.png";

const Navigation = ({ drawer }) => {
  return (
    <div className={`nav ${drawer ? " navDrawer" : " nav__hide"}`}>
      <div className={`nav__logo ${drawer ? " navDrawer__logo" : ""}`}>
        <img src={logo} alt="logo" />
      </div>
      <ul className={`nav__list ${drawer ? " navDrawer__list" : ""}`}>
        <li
          className={`nav__item nav__item--profile ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/profile">
            <span className="navDrawer__item--title">Profile</span>
            <img src={profile} alt="profile" />
          </Link>
        </li>
        <li
          className={`nav__item nav__item--dashboard ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/dashboard">
            <span className="navDrawer__item--title">Dashboard</span>
            <img
              className="nav__itemIcon--regular"
              src={dashboard}
              alt="dashboard"
            />
            <img
              className="nav__itemIcon--hover"
              src={dashboardColor}
              alt="dashboard"
            />
          </Link>
        </li>
        <li
          className={`nav__item nav__item--catalog ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/catalog">
            <span className="navDrawer__item--title">Catalog</span>
            <img
              className="nav__itemIcon--regular"
              src={catalog}
              alt="catalog"
            />
            <img
              className="nav__itemIcon--hover"
              src={catalogColor}
              alt="catalog"
            />
          </Link>
        </li>
        <li
          className={`nav__item nav__item--inventory ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/inventory">
            <span className="navDrawer__item--title">Inventory</span>
            <img
              className="nav__itemIcon--regular"
              src={inventory}
              alt="inventory"
            />
            <img
              className="nav__itemIcon--hover"
              src={inventoryColor}
              alt="inventory"
            />
          </Link>
        </li>
        <li
          className={`nav__item nav__item--cart ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/cart">
            <span className="navDrawer__item--title">Cart</span>
            <img className="nav__itemIcon--regular" src={cart} alt="cart" />
            <img className="nav__itemIcon--hover" src={cartColor} alt="cart" />
          </Link>
        </li>
        <li
          className={`nav__item nav__item--orders ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/orders">
            <span className="navDrawer__item--title">Orders</span>
            <img className="nav__itemIcon--regular" src={orders} alt="orders" />
            <img
              className="nav__itemIcon--hover"
              src={ordersColor}
              alt="orders"
            />
          </Link>
        </li>
        <li
          className={`nav__item nav__item--transactions ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/transactions">
            <span className="navDrawer__item--title">Transactions</span>
            <img
              className="nav__itemIcon--regular"
              src={transactions}
              alt="transactions"
            />
            <img
              className="nav__itemIcon--hover"
              src={transactionsColor}
              alt="transactions"
            />
          </Link>
        </li>
        <li
          className={`nav__item nav__item--storesList ${
            drawer ? " navDrawer__item" : ""
          }`}
        >
          <Link to="/storesList">
            <span className="navDrawer__item--title">Stores List</span>
            <img
              className="nav__itemIcon--regular"
              src={storesList}
              alt="storesList"
            />
            <img
              className="nav__itemIcon--hover"
              src={storesListColor}
              alt="storesList"
            />
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default Navigation;
