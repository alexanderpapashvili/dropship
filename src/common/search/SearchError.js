import "./SearchError.css";
import searchErrorIcon from "../../media/searchError.png";
import { useSelector } from "react-redux";

const SearchError = () => {
  const searchError = useSelector((state) => state.UI.searchError);

  return (
    <div
      className={`searchError--hide ${
        searchError ? " searchError--displayed" : ""
      }`}
    >
      <img src={searchErrorIcon} alt="" />
      <h5>Empty The Search Box</h5>
    </div>
  );
};

export default SearchError;
