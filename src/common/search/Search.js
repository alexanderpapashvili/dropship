import "./Search.css";
import searchIcon from "../../media/searchIcon.png";
import CloseSearch from "../../media/closeForSearch.png";
import { useDispatch, useSelector } from "react-redux";
import { searchResult } from "../../reducers/catalogPageReducers/catalogPageActions";
import { searchQuery as searchQueryAction } from "../../reducers/catalogPageReducers/catalogPageActions";
import { selectAllItems } from "../../reducers/catalogPageReducers/catalogPageActions";
import { selectSingleItem } from "../../reducers/catalogPageReducers/catalogPageActions";
import { searchError } from "../../reducers/UIReducers/UIReducersActions";

const Search = ({ showSearch, setShowSearch }) => {
  const dispatch = useDispatch();
  const searchQuery = useSelector((state) => state.catalogPage.searchQuery);

  const filter = () => {
    let filteredArr;
    filteredArr = JSON.parse(localStorage.getItem("products")).filter((items) =>
      items.title.toLowerCase().includes(searchQuery.toLowerCase())
    );
    if (filteredArr.length === 0) {
      dispatch(searchError(true));
      dispatch(searchResult(filteredArr));
    } else {
      dispatch(searchError(false));
      dispatch(searchResult(filteredArr));
    }
    dispatch(selectAllItems(false));
    dispatch(selectSingleItem(false));
  };

  const queryChange = (e) => {
    dispatch(searchQueryAction(e.target.value));
  };

  const handleEnter = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      filter(e.target.value);
    }
  };
  const show = () => {
    setShowSearch(true);
  };

  const close = () => {
    setShowSearch(false);
  };

  return (
    <div className="search">
      <input
        type="text"
        className={`search__input ${showSearch ? " search__input--show" : ""}`}
        placeholder="search..."
        value={searchQuery}
        onChange={queryChange}
        onKeyDown={handleEnter}
      />
      <button
        className={`search__button ${showSearch ? " search__button-show" : ""}`}
        id="searchButton"
        onClick={filter}
      >
        <img
          src={searchIcon}
          alt="SearchIcon"
          className="search__button--searchIcon"
        />
      </button>
      <button
        className={`search__button--close ${
          showSearch ? " search__button--close--show" : ""
        }`}
        onClick={close}
      >
        <img
          src={CloseSearch}
          alt="closeSearch"
          className="search__button--closeIcon"
        />
      </button>
      <button
        className={`searchShow__button ${
          showSearch ? " searchShow__button--hide" : ""
        }`}
        onClick={show}
      >
        <img src={searchIcon} alt="SearchIcon" />
      </button>
    </div>
  );
};

export default Search;
