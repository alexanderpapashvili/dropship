import "./LoadingGif.css";
import loadingGif from "../media/loading.gif";
import { useSelector } from "react-redux";

const LoadingGif = ({ cart }) => {
  const loadingGifCatalog = useSelector((state) => state.UI.loadingGifCatalog);
  const loadingGifCart = useSelector((state) => state.UI.loadingGifCart);

  return (
    <div
      className={`loadingGif ${
        cart !== true && loadingGifCatalog ? " loadingGif--show" : ""
      } ${cart && loadingGifCart ? " loadingGif--show" : ""}`}
    >
      <img src={loadingGif} alt="Loading..." />
    </div>
  );
};

export default LoadingGif;
