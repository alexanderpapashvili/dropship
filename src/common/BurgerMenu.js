import "./BurgerMenu.css";
import React, { useState } from "react";
import Navigation from "../common/Navigation";
import { Drawer } from "@material-ui/core";

const BurgerMenu = () => {
  const [state, setState] = useState(false);

  const toggleDrawer = (open) => (event) => {
    setState(open);
  };

  return (
    <>
      <div
        className={`burgerMenu ${state ? " change" : ""}`}
        onClick={toggleDrawer(true)}
      >
        <div className="burgerMenu__bar burgerMenu__bar1"></div>
        <div className="burgerMenu__bar burgerMenu__bar2"></div>
        <div className="burgerMenu__bar burgerMenu__bar3"></div>
      </div>
      <Drawer open={state} anchor={"right"} onClose={toggleDrawer(false)}>
        <Navigation drawer></Navigation>
      </Drawer>
    </>
  );
};

export default BurgerMenu;
