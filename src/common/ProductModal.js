import "./ProductModal.css";
import closeIcon from "../media/cancel.png";
import ItemPricing from "./singleItem/ItemPricing";
import AddToCartButton from "./buttons/AddToCartButton";
import { useHistory, useParams } from "react-router-dom";
import { makeStyles, Modal } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getASingleProduct } from "../API";
import { modalOpen } from "../reducers/catalogPageReducers/catalogPageActions";
import loadingGifIcon from "../media/loading.gif";
import { modalLoadingGif as modalLoadingGifAction } from "../reducers/UIReducers/UIReducersActions";

const useStyles = makeStyles(() => ({
  productModal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "none",
  },
}));

const ProductModal = () => {
  const history = useHistory();
  const { productId } = useParams();
  const classes = useStyles();
  const dispatch = useDispatch();

  const [openedProduct, setOpenedProduct] = useState({});
  const modalDisplayed = useSelector(
    (store) => store.catalogPage.modalProductId
  );
  const loadingGifModal = useSelector((state) => state.UI.loadingGifModal);

  useEffect(() => {
    if (productId !== undefined) {
      getASingleProduct(productId).then((res) => {
        setOpenedProduct(res);
        setTimeout(() => {
          dispatch(modalLoadingGifAction(false));
        }, 100);
      });
    }
  }, [productId]);

  const close = () => {
    setOpenedProduct({});
    history.push("/catalog");
    dispatch(modalOpen(false));
    dispatch(modalLoadingGifAction(true));
  };

  return (
    <Modal
      className={classes.productModal}
      open={modalDisplayed ? modalDisplayed : false}
      onClose={close}
    >
      <>
        <div
          className={`loadingGifModal ${
            loadingGifModal ? " loadingGifModal--show" : ""
          }`}
        >
          <img src={loadingGifIcon} alt="Loading..." />
        </div>
        <div
          className={`modal__wrapper ${
            loadingGifModal ? " modal__wrapper--hide" : ""
          }`}
        >
          <div className="modal__leftSide">
            <ItemPricing price={openedProduct.price} />
            <img src={openedProduct.imageUrl} className="modal__image" alt="" />
            <img
              src={openedProduct.imageUrl}
              className="modal__image--small"
              alt=""
            />
          </div>
          <div className="modal__rightSide">
            <div className="modal__itemId">
              <span className="modal__itemCode">SKU# frgx-459517 COPY</span>
              <span className="modal__supplier">
                Supplier: <a href="/#">SP-Supplier115</a>
              </span>
            </div>
            <h2 className="modal__itemTitle">{openedProduct.title}</h2>
            <div className="modal__buttonWrapper">
              <AddToCartButton modal />
            </div>
            <div className="modal__infoWrapper">
              <span>Product Details</span>
              <p className="modal__description">{openedProduct.description}</p>
            </div>
          </div>
          <img
            onClick={close}
            src={closeIcon}
            className="productModal__closeIcon"
            alt=""
          />
        </div>
      </>
    </Modal>
  );
};

export default ProductModal;
