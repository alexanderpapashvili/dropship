import "./ProductsCount.css";
import { useSelector } from "react-redux";

const ProductsCount = () => {
  const products = useSelector(
    (state) => state.catalogPage.catalogProductsList
  );
  const selectedItems = useSelector((state) => state.catalogPage.selectedItems);

  return (
    <div className="selectAll__productsCount">
      <span id="productsCount">
        selected {selectedItems && selectedItems.length} out of
      </span>
      <span>&nbsp;{products && products.length} products</span>
    </div>
  );
};

export default ProductsCount;
