import "./SliderBar.css";
import { Slider, makeStyles } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { filteringItemsByPrice } from "../reducers/catalogPageReducers/catalogPageActions";

const useStyles = makeStyles(() => ({
  slider: {
    color: "#49547d",
  },
}));

const SliderBar = ({ value, setValue, title, max, min, icon, disable }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const fullSizeSidebar = useSelector((state) => state.UI.openFullSizeSidebar);

  const slideBarChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (disable !== true) {
        let filteredArrMinPrice =
          JSON.parse(localStorage.getItem("products")) &&
          JSON.parse(localStorage.getItem("products")).filter(
            (item) => item.price >= value[0]
          );
        let filteredArr =
          filteredArrMinPrice &&
          filteredArrMinPrice.filter((item) => item.price <= value[1]);
        dispatch(filteringItemsByPrice(filteredArr));
      }
    }, 200);
    return () => clearTimeout(timeout);
  }, [value]);

  return (
    <div className="slider__wrapper">
      <div className={`slider ${fullSizeSidebar ? " sliderScreen" : ""}`}>
        <span className="slider__title">{title}</span>
        <Slider
          className={classes.slider}
          max={parseInt(max)}
          min={parseInt(min)}
          value={value}
          valueLabelDisplay="auto"
          aria-labelledby="range-slider"
          onChange={slideBarChange}
        ></Slider>
        <div className="slider__values">
          <div className="startingValue">
            <div>{icon}</div>
            <div>{value[0]}</div>
          </div>
          <div className="endingValue">
            <div>{icon}</div>
            <div>{value[1]}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SliderBar;
