import "./DropDown.css";
import dropDownIcon from "../../media/dropDownIcon.png";
import dropUpIcon from "../../media/dropUpIcon.png";

const DropDown = ({
  dropDown,
  setDropDown,
  dropDownTitle,
  dropDownItems,
  light,
}) => {
  const showSuggestions = () => {
    if (dropDown === true) {
      setDropDown(false);
    } else {
      setDropDown(true);
    }
  };
  const handleCategorySort = () => {};

  return (
    <>
      <div
        className={`sidebar__sort ${light ? " sidebar__sort--light" : ""}`}
        onClick={showSuggestions}
      >
        <h5>{dropDownTitle}</h5>
        <img
          src={dropDownIcon}
          className={`sort__dropDownIcon ${
            dropDown ? " sort__dropDownIcon--hide" : ""
          }`}
          alt=""
        />
        <img
          src={dropUpIcon}
          className={`sort__dropUpIcon ${
            dropDown ? " sort__dropUpIcon--show" : ""
          }`}
          alt=""
        />
      </div>
      <ul className={`sort--list ${dropDown ? " sort--listShow" : ""}`}>
        <li onClick={handleCategorySort} id={dropDownItems[0]}>
          {dropDownItems[0]}
        </li>
        <li onClick={handleCategorySort} id={dropDownItems[1]}>
          {dropDownItems[1]}
        </li>
        <li onClick={handleCategorySort} id={dropDownItems[2]}>
          {dropDownItems[2]}
        </li>
        <li onClick={handleCategorySort} id={dropDownItems[3]}>
          {dropDownItems[3]}
        </li>
        <li onClick={handleCategorySort} id={dropDownItems[4]}>
          {dropDownItems[4]}
        </li>
        <div className={`${light ? " sort__listBorder" : ""}`}></div>
      </ul>
    </>
  );
};

export default DropDown;
