import "./SideBar.css";
import React, { useState } from "react";
import DropDown from "./DropDown";
import ResetFilterButton from "../buttons/ResetFilterButton";
import SliderBar from "../SliderBar";
import SidebarBackButton from "../buttons/SidebarBackButton";
import FakeDropDown from "../FakeDropDown";
import { useSelector } from "react-redux";

const SideBar = () => {
  const [dropDownFirst, setDropDownFirst] = useState(false);
  const [dropDownSecond, setDropDownSecond] = useState(false);
  const [priceRange, setPriceRange] = useState([0, 10000]);
  const [profitRange, setProfitRange] = useState([3, 98]);

  const fullSizeSidebar = useSelector((state) => state.UI.openFullSizeSidebar);
  const modalDisplayed = useSelector(
    (store) => store.catalogPage.modalProductId
  );

  return (
    <aside
      className={`sideBar ${modalDisplayed ? " sideBar--modal" : ""} ${
        fullSizeSidebar ? " sideBarScreen" : ""
      }`}
    >
      <div className="sidebar__menu">
        <DropDown
          dropDown={dropDownFirst}
          setDropDown={setDropDownFirst}
          dropDownTitle="Choose Niche"
          dropDownItems={[
            "...",
            "*Best Sellers",
            "Beauty",
            "Fashion",
            "Furniture",
          ]}
        />
        <DropDown
          dropDown={dropDownSecond}
          setDropDown={setDropDownSecond}
          dropDownTitle="Choose Category"
          dropDownItems={[
            "All Products",
            "Men's Clothing",
            "Women's Clothing",
            "Jewelery",
            "Electronics",
          ]}
          light
        />
        <SidebarBackButton />
        <FakeDropDown title="ship from" />
        <FakeDropDown title="ship to" />
        <FakeDropDown title="select supplier" />
        <SliderBar
          value={priceRange}
          setValue={setPriceRange}
          title="Price Range"
          max="10000"
          min="0"
          icon="$"
        />
        <SliderBar
          value={profitRange}
          setValue={setProfitRange}
          title="Profit Range"
          max="98"
          min="3"
          icon="%"
          disable
        />
        <ResetFilterButton
          dropDownFirst={dropDownFirst}
          dropDownSecond={dropDownSecond}
          setDropDownFirst={setDropDownFirst}
          setDropDownSecond={setDropDownSecond}
          valueOne={priceRange}
          setValueOne={setPriceRange}
          valueTwo={profitRange}
          setValueTwo={setProfitRange}
        />
      </div>
    </aside>
  );
};

export default SideBar;
