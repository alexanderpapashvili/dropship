import "./FakeDropDown.css";
import dropDownIconBlack from "../media/dropDownIconBlack.png";

const FakeDropDown = ({ title }) => {
  return (
    <div className="fakeDropDown__wrapper">
      <div className="fakeDropDown">
        <span> {title}</span>
        <img src={dropDownIconBlack} alt="" />
      </div>
    </div>
  );
};

export default FakeDropDown;
