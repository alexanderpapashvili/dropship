import "./ItemPricing.css";

const ItemPricing = ({ price }) => {
  return (
    <div className="item__pricing">
      <span className="item__pricing--unit item__pricing--RRP">
        $25 <span>RRP</span>
      </span>
      <span className="item__pricing--unit item__pricing--cost">
        ${price} <span>Cost</span>
      </span>
      <span className="item__pricing--unit item__pricing--profit">
        20%($5) <span>Profit</span>
      </span>
    </div>
  );
};

export default ItemPricing;
