import "./SingleItem.css";
import EditProductButton from "../../common/buttons/EditProductButton";
import DeleteProductButton from "../../common/buttons/DeleteProductButton";
import AddToCartButton from "../buttons/AddToCartButton";
import { useState, useEffect } from "react";
import ItemPricing from "./ItemPricing";
import { Grid } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  modalOpen,
  selectSingleItem as selectSingleItemAction,
  selectedItemsArray,
} from "../../reducers/catalogPageReducers/catalogPageActions";

const SingleItem = ({ id, title, price, image }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const modalDisplayed = useSelector(
    (store) => store.catalogPage.modalProductId
  );
  const selectAll = useSelector((store) => store.catalogPage.selectAll);
  const selectSingleItem = useSelector(
    (store) => store.catalogPage.selectSingleItem
  );
  const selectedItems = useSelector((store) => store.catalogPage.selectedItems);

  const [checkboxSelected, setCheckboxSelected] = useState(selectAll);

  const clickOnCheckbox = (e) => {
    e.stopPropagation();
  };

  const checked = selectedItems?.find((item) => item.id === id);

  const checkboxChange = (e) => {
    if (e.target.checked) {
      const found = JSON.parse(localStorage.getItem("products")).find(
        (item) => item.id === id
      );
      dispatch(selectedItemsArray([...selectedItems, found]));
    } else {
      const arr = selectedItems?.filter((item) => item.id !== id);
      dispatch(selectedItemsArray(arr));
    }
  };

  useEffect(() => {
    setCheckboxSelected(selectAll);
  }, [selectAll]);

  useEffect(() => {
    if (selectSingleItem) {
      dispatch(selectSingleItemAction(true));
    } else {
      setCheckboxSelected(selectSingleItem);
    }
  }, [selectSingleItem]);

  const OpenModal = (itemId) => {
    history.push(`/catalog/${itemId}`);
    dispatch(modalOpen(true));
  };

  return (
    <Grid item xs={12} sm={6} md={4} lg={3}>
      <div
        className={`item ${checked ? " item--selected" : ""} ${
          modalDisplayed ? " item--modal" : ""
        }`}
        onClick={() => OpenModal(id)}
      >
        <div
          onClick={clickOnCheckbox}
          className={`item__checkbox ${
            checked ? " item__checkbox--checked" : ""
          } ${modalDisplayed ? " item__checkbox--modal" : ""}`}
        >
          <label
            className={`container ${modalDisplayed ? "container--modal" : ""}`}
          >
            <input
              type="checkbox"
              checked={checked}
              onChange={checkboxChange}
            />
            <span className="checkMark"></span>
          </label>
        </div>
        <div
          className={`item__addToCart ${
            modalDisplayed ? " item__addToCart--modal" : ""
          }`}
        >
          <DeleteProductButton id={id} />
          <EditProductButton id={id} />
          <AddToCartButton itemHover itemId={id} />
        </div>
        <div className="item__image">
          <img src={image} alt="product" />
        </div>
        <div className="item__title">
          <h3>{title}</h3>
        </div>
        <div className="item__supplier">
          By: <a href="/#">SP-Supplier115</a>
        </div>
        <div className="item__price--wrapper">
          <ItemPricing price={price} />
        </div>
      </div>
    </Grid>
  );
};

export default SingleItem;
