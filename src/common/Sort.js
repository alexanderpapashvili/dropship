import "./Sort.css";
import sortIcon from "../media/sort-button-with-three-lines.png";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { sortResult } from "../reducers/catalogPageReducers/catalogPageActions";
import { selectAllItems } from "../reducers/catalogPageReducers/catalogPageActions";
import { selectSingleItem } from "../reducers/catalogPageReducers/catalogPageActions";

const Sort = () => {
  const dispatch = useDispatch();
  const products = useSelector(
    (state) => state.catalogPage.catalogProductsList
  );

  const sortProduct = (value, array) => {
    if (value === "asc") {
      array.sort((a, b) => a.price - b.price);
    } else if (value === "desc") {
      array.sort((a, b) => b.price - a.price);
    } else if (value === "alphabetic") {
      array.sort((a, b) => a.title.localeCompare(b.title));
    } else if (value === "reverse-alphabetic") {
      array.sort((a, b) => b.title.localeCompare(a.title));
    } else if (value === "default") {
      array.sort((a, b) => a.id - b.id);
    } else {
      array = JSON.parse(localStorage.getItem("products"));
    }
    dispatch(sortResult(array));
    dispatch(selectAllItems(false));
    dispatch(selectSingleItem(false));
  };

  const handleSort = (e) => {
    sortProduct(e.target.value, [...products]);
  };

  return (
    <section className="header__sort">
      <img src={sortIcon} alt="" />
      <select id="sort" onChange={handleSort}>
        <option value="default">Sort By: New Arrivals</option>
        <option value="asc">Sort By: Price: Low to High</option>
        <option value="desc">Sort By: Price: High to Low</option>
        <option value="alphabetic">Sort By: Alphabet</option>
        <option value="reverse-alphabetic">Sort By: Reverse Alphabet</option>
      </select>
    </section>
  );
};

export default Sort;
