export const catalogLoadingGif = (bool) => {
  return {
    type: "CATALOG_LOADING_GIF",
    payload: bool,
  };
};
export const cartLoadingGif = (bool) => {
  return {
    type: "CART_LOADING_GIF",
    payload: bool,
  };
};
export const modalLoadingGif = (bool) => {
  return {
    type: "MODAL_LOADING_GIF",
    payload: bool,
  };
};
export const editLoadingGif = (bool) => {
  return {
    type: "EDIT_LOADING_GIF",
    payload: bool,
  };
};
export const searchError = (bool) => {
  return {
    type: "SEARCH_ERROR",
    payload: bool,
  };
};
export const openFullSizeSidebar = (bool) => {
  return {
    type: "OPEN_FULL_SIZE_SIDEBAR",
    payload: bool,
  };
};
export const openProductMod = (bool) => {
  return {
    type: "PRODUCT_ADD_OR_EDIT",
    payload: bool,
  };
};
