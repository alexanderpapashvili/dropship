const initialState = {
  loadingGifCatalog: true,
  loadingGifCart: true,
  loadingGifModal: true,
  loadingGifEdit: false,
  searchError: false,
  openFullSizeSidebar: false,
  productEditDisplayed: false,
};

const UIReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CATALOG_LOADING_GIF":
      return { ...state, loadingGifCatalog: action.payload };
    case "CART_LOADING_GIF":
      return { ...state, loadingGifCart: action.payload };
    case "MODAL_LOADING_GIF":
      return { ...state, loadingGifModal: action.payload };
    case "EDIT_LOADING_GIF":
      return { ...state, loadingGifEdit: action.payload };
    case "SEARCH_ERROR":
      return { ...state, searchError: action.payload };
    case "OPEN_FULL_SIZE_SIDEBAR":
      return { ...state, openFullSizeSidebar: action.payload };
    case "PRODUCT_ADD_OR_EDIT":
      return { ...state, productEditDisplayed: action.payload };
    default:
      return state;
  }
};

export default UIReducer;
