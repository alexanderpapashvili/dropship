export const catalogItemsRequest = (arr) => {
  return {
    type: "PRODUCTS_FETCHED",
    payload: arr,
  };
};
export const filteringItemsByPrice = (arr) => {
  return {
    type: "FILTERING__PRODUCTS_BY_PRICE",
    payload: arr,
  };
};
export const resetingFilters = (arr) => {
  return {
    type: "FILTER_RESET",
    payload: arr,
  };
};
export const searchResult = (arr) => {
  return {
    type: "SEARCH_RESULT",
    payload: arr,
  };
};
export const sortResult = (arr) => {
  return {
    type: "SORTING__ITEMS",
    payload: arr,
  };
};
export const modalOpen = (bool) => {
  return {
    type: "OPEN_MODAL",
    payload: bool,
  };
};
export const searchQuery = (string) => {
  return {
    type: "SEARCH_QUERY",
    payload: string,
  };
};
export const selectAllItems = (bool) => {
  return {
    type: "SELECT_ALL_ITEMS",
    payload: bool,
  };
};
export const selectSingleItem = (bool) => {
  return {
    type: "SELECT_SINGLE_ITEM",
    payload: bool,
  };
};
export const selectedItemsArray = (arr) => {
  return {
    type: "SELECTED_ITEMS_ARRAY",
    payload: arr,
  };
};
