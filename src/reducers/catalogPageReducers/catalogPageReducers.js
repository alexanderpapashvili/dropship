const initialState = {
  catalogProductsList: [],
  modalProductId: null,
  searchQuery: "",
  selectAll: false,
  selectSingleItem: null,
  selectedItems: [],
};

const CatalogPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "PRODUCTS_FETCHED":
    case "FILTERING__PRODUCTS_BY_PRICE":
    case "FILTER_RESET":
    case "SEARCH_RESULT":
    case "SORTING__ITEMS":
      return { ...state, catalogProductsList: action.payload };
    case "OPEN_MODAL":
      return { ...state, modalProductId: action.payload };
    case "SEARCH_QUERY":
      return { ...state, searchQuery: action.payload };
    case "SELECT_ALL_ITEMS":
      return { ...state, selectAll: action.payload };
    case "SELECT_SINGLE_ITEM":
      return { ...state, selectSingleItem: action.payload };
    case "SELECTED_ITEMS_ARRAY":
      return { ...state, selectedItems: action.payload };
    default:
      return state;
  }
};

export default CatalogPageReducer;
