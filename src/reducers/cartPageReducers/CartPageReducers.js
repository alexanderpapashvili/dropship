const initialState = {
  cartProductsList: [],
};

const CartPageReducers = (state = initialState, action) => {
  switch (action.type) {
    case "CART_PRODUCTS_FETCHED":
      return { ...state, cartProductsList: action.payload };
    default:
      return state;
  }
};

export default CartPageReducers;
