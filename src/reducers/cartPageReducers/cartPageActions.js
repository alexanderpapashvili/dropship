export const cartItemsRequest = (arr) => {
  return {
    type: "CART_PRODUCTS_FETCHED",
    payload: arr,
  };
};
