import "./LandingPageHeader.css";
import LandingPageBurgerMenu from "./LandingPageBurgerMenu";
import facebookWhiteLogo from "../../media/facebookWhite.png";
import facebookLogo from "../../media/facebook.png";
import logo from "../../media/dropShipWhiteLogo.png";
import { useHistory } from "react-router-dom";
import signUpIcon from "../../media/signUpIcon.png";
import logInIcon from "../../media/logInIcon.png";

const LandingPageHeader = () => {
  const history = useHistory();

  const openLogInPage = () => {
    history.push("/logIn");
  };

  const openSignUpPage = () => {
    history.push("/signUp");
  };

  return (
    <div className="landingPageHeader">
      <div className="landingPageHeader__container">
        <div className="landingPageHeader__logo">
          <img src={logo} alt="logo" />
        </div>
        <div className="landingPageHeader__navigation">
          <ul className="navigation__list">
            <li>about</li>
            <li>catalog</li>
            <li>pricing</li>
            <li>suppliers</li>
            <li>help center</li>
            <li>blog</li>
          </ul>
          <LandingPageBurgerMenu />
          <button
            className="landingPageHeader__signUpButton"
            onClick={openSignUpPage}
          >
            Sign Up NOW
          </button>
          <button
            className="landingPageHeader__signUpButton--icon"
            onClick={openSignUpPage}
          >
            <img src={signUpIcon} alt="SignUp" />
          </button>
          <button
            className="landingPageHeader__logInButton"
            onClick={openLogInPage}
          >
            Log In
          </button>
          <button
            className="landingPageHeader__logInButton--icon"
            onClick={openLogInPage}
          >
            <img src={logInIcon} alt="logIn" />
          </button>
          <div className="landingPageHeader__facebook">
            <img
              src={facebookWhiteLogo}
              className="landingPageHeader__facebookIcon--white"
              alt=""
            />
            <img
              src={facebookLogo}
              className="landingPageHeader__facebookIcon"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingPageHeader;
