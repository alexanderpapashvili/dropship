import "./LandingPageBurgerMenu.css";
import { Drawer } from "@material-ui/core";
import React, { useState } from "react";
import cancelIcon from "../../media/cancelWhite.png";

const LandingPageBurgerMenu = () => {
  const [openDrawer, setOpenDrawer] = useState(false);

  const toggleDrawer = (open) => (event) => {
    setOpenDrawer(open);
  };

  return (
    <>
      <div
        className={`landingPageBurgerMenu ${openDrawer ? " change" : ""}`}
        onClick={toggleDrawer(true)}
      >
        <div className="landingPageBurgerMenu__bar landingPageBurgerMenu__bar1"></div>
        <div className="landingPageBurgerMenu__bar landingPageBurgerMenu__bar2"></div>
        <div className="landingPageBurgerMenu__bar landingPageBurgerMenu__bar3"></div>
      </div>
      <Drawer open={openDrawer} anchor={"right"} onClose={toggleDrawer(false)}>
        <div className="landingPageDrawer">
          <div className="landingPageDrawer__closeIcon">
            <img src={cancelIcon} alt="close" onClick={toggleDrawer(false)} />
          </div>
          <ul className="landingPageDrawer__list">
            <li>about</li>
            <li>catalog</li>
            <li>pricing</li>
            <li>suppliers</li>
            <li>help center</li>
            <li>blog</li>
          </ul>
        </div>
      </Drawer>
    </>
  );
};

export default LandingPageBurgerMenu;
