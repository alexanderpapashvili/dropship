import "./SignUp.css";
import { useEffect } from "react";
import * as yup from "yup";
import { Button, makeStyles, TextField } from "@material-ui/core";
import logo from "../../media/Logo.png";
import { useHistory, useRouteMatch } from "react-router-dom";
import { signUp } from "../../API";
import { useSnackbar } from "notistack";
import { Form, Formik, Field, ErrorMessage } from "formik";
import InputAdornment from "@material-ui/core/InputAdornment";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import TitleIcon from "@material-ui/icons/Title";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import googleIcon from "../../media/google.svg";
import facebookIcon from "../../media/facebook.svg";

const useStyles = makeStyles(() => ({
  inputField: {
    width: "85%",
    height: "47px",
    color: "#61d4df",
    marginTop: "35px",
  },
  signUpButton: {
    color: "white",
    fontFamily: "'Poppins', sans-serif",
    fontSize: "16px",
    fontWeight: "600",
    backgroundColor: "#61d4df",
    margin: "auto",
    width: "85%",
    height: "44px",
    textTransform: "capitalize",
    marginTop: "20px",
    cursor: "pointer",
  },
}));

const validationSchema = yup.object().shape({
  firstName: yup.string().required(),
  lastName: yup.string().required(),
  email: yup.string().required().email(),
  password: yup
    .string()
    .required()
    .min(5, "Password is too short")
    .max(20, "Password is too long"),
});

const SignUp = () => {
  const classes = useStyles();
  const history = useHistory();
  let { url } = useRouteMatch();
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token !== null) {
      history.push(`${url}`);
    } else {
      history.push("/signUp");
    }
  }, []);

  const performSignUp = (values) => {
    signUp(
      values.firstName,
      values.lastName,
      values.email,
      values.password,
      values.password
    )
      .then((result) => {
        history.push("/catalog");
        enqueueSnackbar("Congratulations! You successfully signed up.", {
          variant: "success",
        });
      })
      .catch((err) => {
        enqueueSnackbar("Something went wrong. Please try again!", {
          variant: "error",
        });
      });
  };

  const costumeInputFirstName = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.firstName && errors.firstName ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="First Name"
        id="input-with-icon-textField"
        placeholder="John"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <TitleIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputLastName = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.lastName && errors.lastName ? true : false}
        className={classes.inputField}
        placeholder="Doe"
        variant="outlined"
        label="Last Name"
        id="input-with-icon-textField"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <TitleIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputEmail = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.email && errors.email ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="Email"
        id="input-with-icon-textField"
        placeholder="johndoe@example.com"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <MailOutlineIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputPassword = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.password && errors.password ? true : false}
        className={classes.inputField}
        placeholder="••••••••••"
        type="password"
        variant="outlined"
        label="Password"
        id="input-with-icon-textField"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKeyIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const redirecting = () => {
    history.push("/logIn");
  };

  return (
    <div className="signUp">
      <div className="signUp__container">
        <div className="signUp__title">
          <img src={logo} alt="logo" />
          <h3>Members Sign Up</h3>
        </div>
        <Formik
          enableReinitialize
          initialValues={{
            firstName: "",
            lastName: "",
            email: "",
            password: "",
          }}
          onSubmit={performSignUp}
          validationSchema={validationSchema}
        >
          <Form className="signUp__form">
            <Field name="firstName" component={costumeInputFirstName} />
            <ErrorMessage
              className="errorMessage"
              name="firstName"
              component="div"
            />
            <Field name="lastName" component={costumeInputLastName} />
            <ErrorMessage
              className="errorMessage"
              name="lastName"
              component="div"
            />
            <Field name="email" component={costumeInputEmail} />
            <ErrorMessage
              className="errorMessage"
              name="email"
              component="div"
            />
            <Field name="password" component={costumeInputPassword} />
            <ErrorMessage
              className="errorMessage"
              name="password"
              component="div"
            />
            <Button
              variant="contained"
              className={classes.signUpButton}
              type="submit"
            >
              Sign Up
            </Button>
          </Form>
        </Formik>
        <p>------------------- Or Sign Up With -------------------</p>
        <div className="icons">
          <img src={googleIcon} alt="google" />
          <img src={facebookIcon} alt="facebook" />
        </div>
        <p id="signUp__text" onClick={redirecting}>
          Already have an account? <strong> Log In</strong>
        </p>
      </div>
    </div>
  );
};

export default SignUp;
