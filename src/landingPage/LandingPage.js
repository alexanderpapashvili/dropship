import "./LandingPage.css";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import LandingPageHeader from "./header/LandingPageHeader";

const LogInAndSignUpPage = () => {
  const history = useHistory();

  useEffect(() => {
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("user");
    if (token !== null || user !== null) {
      history.push("/catalog");
    }
  }, []);

  return (
    <div className="landingPage">
      <LandingPageHeader />
    </div>
  );
};

export default LogInAndSignUpPage;
