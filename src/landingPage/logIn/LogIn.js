import "./LogIn.css";
import { useEffect } from "react";
import * as yup from "yup";
import { Button, makeStyles, TextField } from "@material-ui/core";
import { logIn } from "../../API";
import { useHistory, useRouteMatch } from "react-router-dom";
import { Form, Formik, Field, ErrorMessage } from "formik";
import InputAdornment from "@material-ui/core/InputAdornment";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import { useSnackbar } from "notistack";
import logo from "../../media/Logo.png";
import googleIcon from "../../media/google.svg";
import facebookIcon from "../../media/facebook.svg";

const useStyles = makeStyles((theme) => ({
  inputField: {
    width: "85%",
    height: "47px",
    color: "#61d4df",
    marginTop: "25px",
  },
  logInButton: {
    color: "white",
    fontFamily: "'Poppins', sans-serif",
    fontSize: "16px",
    fontWeight: "600",
    backgroundColor: "#61d4df",
    margin: "auto",
    width: "85%",
    height: "44px",
    textTransform: "capitalize",
    marginTop: "20px",
    cursor: "pointer",
  },
}));

const validationSchema = yup.object().shape({
  email: yup.string().required().email(),
  password: yup
    .string()
    .required()
    .min(5, "Password is too short")
    .max(20, "Password is too long"),
});

const LogIn = () => {
  const classes = useStyles();
  const history = useHistory();
  let { url } = useRouteMatch();
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token !== null) {
      history.push(`${url}`);
    } else {
      history.push("/logIn");
    }
  }, []);

  const performLogIn = (values) => {
    logIn(values.email, values.password)
      .then((result) => {
        history.push("/catalog");
        enqueueSnackbar("You are logged In", { variant: "success" });
      })
      .catch((err) => {
        enqueueSnackbar("Incorrect email or password!", { variant: "error" });
      });
  };

  const costumeInputEmail = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.email && errors.email ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="Email"
        id="input-with-icon-textField"
        placeholder="johndoe@example.com"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <MailOutlineIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputPassword = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.password && errors.password ? true : false}
        className={classes.inputField}
        placeholder="••••••••••"
        type="password"
        variant="outlined"
        label="Password"
        id="input-with-icon-textField"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <VpnKeyIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const redirecting = () => {
    history.push("/signUp");
  };

  return (
    <div className="logIn">
      <div className="logIn__container">
        <div className="logIn__title">
          <img src={logo} alt="logo" />
          <h3>Members Log In</h3>
        </div>
        <Formik
          enableReinitialize
          initialValues={{ email: "", password: "" }}
          onSubmit={performLogIn}
          validationSchema={validationSchema}
        >
          <Form className="form">
            <Field name="email" component={costumeInputEmail} />
            <ErrorMessage
              className="errorMessage"
              name="email"
              component="div"
            />
            <Field name="password" component={costumeInputPassword} />
            <ErrorMessage
              className="errorMessage"
              name="password"
              component="div"
            />
            <span className="logIn__forgetPassword">Forgot password?</span>
            <Button
              variant="contained"
              className={classes.logInButton}
              type="submit"
            >
              Log In
            </Button>
          </Form>
        </Formik>
        <p>------------------- Or Log In With -------------------</p>
        <div className="icons">
          <img src={googleIcon} alt="google" />
          <img src={facebookIcon} alt="facebook" />
        </div>
        <p id="logIn__text" onClick={redirecting}>
          Don't have an account? <strong> Sign Up</strong>
        </p>
      </div>
    </div>
  );
};

export default LogIn;
