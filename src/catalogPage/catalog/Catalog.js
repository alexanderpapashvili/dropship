import "./Catalog.css";
import SingleItem from "../../common/singleItem/SingleItem";
import ProductModal from "../../common/ProductModal";
import LoadingGif from "../../common/LoadingGif";
import SearchError from "../../common/search/SearchError";
import { Grid } from "@material-ui/core";
import { useSelector } from "react-redux";
import AddOrEditProduct from "../addOrEditProduct/AddOrEditProduct";

const Catalog = () => {
  const productList = useSelector(
    (state) => state.catalogPage.catalogProductsList
  );
  const loadingGifCatalog = useSelector((state) => state.UI.loadingGifCatalog);
  const searchError = useSelector((state) => state.UI.searchError);

  return (
    <>
      <LoadingGif />
      <ProductModal />
      <AddOrEditProduct />
      <section
        className={`catalog ${searchError ? " catalog--hide" : ""} ${
          loadingGifCatalog ? " catalog--hide" : ""
        }`}
        id="catalog"
      >
        <Grid container spacing={3}>
          {productList &&
            productList.map((item) => (
              <SingleItem
                key={item.id}
                id={item.id}
                title={item.title}
                category={item.category}
                price={item.price}
                image={item.imageUrl}
              />
            ))}
        </Grid>
      </section>
      <SearchError />
    </>
  );
};

export default Catalog;
