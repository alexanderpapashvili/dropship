import "./CatalogPage.css";
import Navigation from "../common/Navigation";
import CatalogHeader from "./header/CatalogHeader";
import Main from "./main/Main";
import SideBar from "../common/sideBar/SideBar";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { products } from "../API";
import { useSelector, useDispatch } from "react-redux";
import {
  catalogItemsRequest,
  selectAllItems,
  selectSingleItem,
  selectedItemsArray,
} from "../reducers/catalogPageReducers/catalogPageActions";
import { catalogLoadingGif } from "../reducers/UIReducers/UIReducersActions";

const CatalogPage = () => {
  const history = useHistory();

  const fullSizeSidebar = useSelector((state) => state.UI.openFullSizeSidebar);

  useEffect(() => {
    const token = localStorage.getItem("token");
    const user = localStorage.getItem("user");
    if (token === null || user === null) {
      history.push("/");
    }
  }, []);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(selectAllItems(false));
    dispatch(selectSingleItem(false));
    dispatch(selectedItemsArray([]));
    dispatch(catalogLoadingGif(true));
    products()
      .then((result) => {
        localStorage.setItem("products", JSON.stringify(result));
        dispatch(catalogItemsRequest(result));
      })
      .finally(() => {
        dispatch(catalogLoadingGif(false));
      });
  }, []);

  return (
    <>
      <Navigation />
      <div className="body__wrapper">
        <SideBar />
        <div
          className={`catalogHeaderAndMain ${
            fullSizeSidebar ? " catalogHeaderAndMain--hide" : ""
          }`}
        >
          <CatalogHeader />
          <Main />
        </div>
      </div>
    </>
  );
};

export default CatalogPage;
