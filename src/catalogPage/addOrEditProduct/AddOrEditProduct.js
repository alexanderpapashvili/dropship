import "./AddOrEditProduct.css";
import logo from "../../media/Logo.png";
import * as yup from "yup";
import {
  addProduct,
  getASingleProduct,
  editProduct,
  products,
} from "../../API";
import { useState, useEffect } from "react";
import { useSnackbar } from "notistack";
import { Form, Formik, Field, ErrorMessage } from "formik";
import InputAdornment from "@material-ui/core/InputAdornment";
import TitleIcon from "@material-ui/icons/Title";
import DescriptionIcon from "@material-ui/icons/Description";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import ImageIcon from "@material-ui/icons/Image";
import { Modal, Button, makeStyles, TextField } from "@material-ui/core";
import { openProductMod } from "../../reducers/UIReducers/UIReducersActions";
import { useDispatch, useSelector } from "react-redux";
import { catalogItemsRequest } from "../../reducers/catalogPageReducers/catalogPageActions";
import { catalogLoadingGif } from "../../reducers/UIReducers/UIReducersActions";
import loadingGifIcon from "../../media/loading.gif";
import { editLoadingGif as editLoadingGifAction } from "../../reducers/UIReducers/UIReducersActions";

const useStyles = makeStyles(() => ({
  productModal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "none",
  },
  inputField: {
    width: "85%",
    height: "47px",
    color: "#61d4df",
    marginTop: "35px",
  },
  descriptionInputField: {
    width: "85%",
    height: "47px",
    color: "#61d4df",
    margin: "35px 0 170px 0",
  },
  saveButton: {
    color: "white",
    fontFamily: "'Poppins', sans-serif",
    fontSize: "16px",
    fontWeight: "600",
    backgroundColor: "#61d4df",
    margin: "auto",
    width: "85%",
    height: "44px",
    textTransform: "capitalize",
    marginTop: "20px",
    cursor: "pointer",
  },
}));

const validationSchema = yup.object().shape({
  title: yup.string().required().min(2).max(100),
  description: yup.string().required().min(5).max(1000),
  price: yup.number().integer().required().min(2),
  imageUrl: yup.string().required().url(),
});

const AddOrEditProduct = () => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const productEditDisplayed = useSelector(
    (state) => state.UI.productEditDisplayed
  );
  const loadingGifEdit = useSelector((state) => state.UI.loadingGifEdit);
  const [product, setProduct] = useState({});

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user"));
    if (user.data.data.isAdmin !== true) {
      window.location.href = "/catalog";
    }
  }, []);

  useEffect(() => {
    if (typeof productEditDisplayed !== "boolean") {
      dispatch(editLoadingGifAction(true));
      getASingleProduct(productEditDisplayed).then((res) => {
        setProduct(res);
        setTimeout(() => {
          dispatch(editLoadingGifAction(false));
        }, 100);
      });
    }
  }, [productEditDisplayed]);

  const handleSubmit = (values) => {
    if (typeof productEditDisplayed !== "boolean") {
      editProduct(productEditDisplayed, values).then((res) => {
        enqueueSnackbar("Product edited successfully!", {
          variant: "success",
        });
        dispatch(openProductMod(false));
        setProduct({});
        products().then((result) => {
          dispatch(catalogItemsRequest(result));
        });
      });
    } else {
      addProduct(values)
        .then((res) => {
          enqueueSnackbar("Product added successfully!", {
            variant: "success",
          });
          dispatch(openProductMod(false));
          setProduct({});
          dispatch(catalogLoadingGif(true));
          products()
            .then((result) => {
              dispatch(catalogItemsRequest(result));
            })
            .finally(() => {
              dispatch(catalogLoadingGif(false));
            });
        })
        .catch((err) => {
          enqueueSnackbar({ err }, { variant: "error" });
        });
    }
  };

  const costumeInputTitle = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.title && errors.title ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="Title"
        id="input-with-icon-textField"
        placeholder="Coca-Cola"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <TitleIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputDescription = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.description && errors.description ? true : false}
        className={classes.descriptionInputField}
        variant="outlined"
        label="Description"
        id="input-with-icon-textField"
        multiline
        rows={10}
        placeholder="Coca-Cola, or Coke, is a carbonated soft drink manufactured by The Coca-Cola Company. Originally marketed as a temperance drink and intended as a patent medicine, it was invented in the late 19th century by John Stith Pemberton and was bought out by businessman Asa Griggs Candler, whose marketing tactics led Coca-Cola to its dominance of the world soft-drink market throughout the 20th century. The drink's name refers to two of its original ingredients: coca leaves, and kola nuts (a source of caffeine). The current formula of Coca-Cola remains a trade secret; however, a variety of reported recipes and experimental recreations have been published. The drink has inspired imitators and created a whole classification of soft drink: colas."
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <DescriptionIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputPrice = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.price && errors.price ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="Price"
        id="input-with-icon-textField"
        placeholder="23"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <AttachMoneyIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const costumeInputImageURL = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.imageUrl && errors.imageUrl ? true : false}
        className={classes.inputField}
        variant="outlined"
        label="imageURL"
        id="input-with-icon-textField"
        placeholder="https://marketingweek.imgix.net/content/uploads/2019/12/16145201/shutterstock_1577827573.jpg?auto=compress,format&q=60&w=736&h=429"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <ImageIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const close = () => {
    dispatch(openProductMod(false));
    setProduct({});
  };

  return (
    <Modal
      className={classes.productModal}
      open={productEditDisplayed ? productEditDisplayed : false}
      onClose={close}
    >
      <>
        <div
          className={`loadingGifEdit ${
            loadingGifEdit ? " loadingGifEdit--show" : ""
          }`}
        >
          <img src={loadingGifIcon} alt="Loading..." />
        </div>
        <div
          className={`AddOrEditProduct__container ${
            loadingGifEdit ? " AddOrEditProduct__container--hide" : ""
          }`}
        >
          <div className="AddOrEditProduct__title">
            <img src={logo} alt="logo" />
            <h3>
              {typeof productEditDisplayed !== "boolean"
                ? "Edit Product"
                : "Add Product"}
            </h3>
          </div>
          <Formik
            enableReinitialize
            initialValues={
              typeof productEditDisplayed !== "boolean"
                ? {
                    title: product.title,
                    description: product.description,
                    price: product.price,
                    imageUrl: product.imageUrl,
                  }
                : {
                    title: "",
                    description: "",
                    price: "",
                    imageUrl: "",
                  }
            }
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <Form className="form">
              <Field name="title" component={costumeInputTitle} />
              <ErrorMessage
                className="errorMessage"
                name="title"
                component="div"
              />
              <Field name="description" component={costumeInputDescription} />
              <ErrorMessage
                className="errorMessage"
                name="description"
                component="div"
              />
              <Field name="price" component={costumeInputPrice} />
              <ErrorMessage
                className="errorMessage"
                name="price"
                component="div"
              />
              <Field name="imageUrl" component={costumeInputImageURL} />
              <ErrorMessage
                className="errorMessage"
                name="imageUrl"
                component="div"
              />
              <Button
                variant="contained"
                className={classes.saveButton}
                type="submit"
              >
                Save
              </Button>
            </Form>
          </Formik>
        </div>
      </>
    </Modal>
  );
};

export default AddOrEditProduct;
