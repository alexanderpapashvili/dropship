import "./CatalogHeader.css";
import HeaderUpperSection from "./HeaderUpperSection";
import Sort from "../../common/Sort";
import { useSelector } from "react-redux";

const CatalogHeader = () => {
  const modalDisplayed = useSelector(
    (store) => store.catalogPage.modalProductId
  );

  return (
    <header className={`header ${modalDisplayed ? " header--modal" : ""}`}>
      <HeaderUpperSection />
      <Sort />
    </header>
  );
};

export default CatalogHeader;
