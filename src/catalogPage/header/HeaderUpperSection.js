import "./HeaderUpperSection.css";
import HeaderSelectAll from "./HeaderSelectAll";
import HeaderSearchAndAddToInventory from "./HeaderSearchAndAddToInventory";

const HeaderUpperSection = () => {
  return (
    <section className="header__upperSection">
      <HeaderSelectAll />
      <HeaderSearchAndAddToInventory />
    </section>
  );
};

export default HeaderUpperSection;
