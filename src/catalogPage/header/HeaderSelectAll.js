import "./HeaderSelectAll.css";
import ProductsCount from "../../common/ProductsCount";
import SelectAllButton from "../../common/buttons/SelectAllButton";
import ClearSelectedButton from "../../common/buttons/ClearSelectedButton";
import SelectAllAndDeselectButton from "../../common/buttons/SelectAllAndDeselectButton";
import FilterButton from "../../common/buttons/FilterButton";

const HeaderSelectAll = () => {
  return (
    <div className="header__selectAll">
      <SelectAllButton />
      <FilterButton />
      <FilterButton small />
      <ProductsCount />
      <SelectAllAndDeselectButton />
      <ClearSelectedButton />
    </div>
  );
};

export default HeaderSelectAll;
