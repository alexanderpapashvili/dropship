import "./HeaderSearchAndAddToInventory.css";
import Search from "../../common/search/Search";
import AddToCartButton from "../../common/buttons/AddToCartButton";
import HelpIcon from "../../media/icons/HelpIcon";
import BurgerMenu from "../../common/BurgerMenu";
import { useState } from "react";
import AddToCartButtonSmall from "../../common/buttons/AddToCartButtonSmall";
import AddProductButton from "../../common/buttons/AddProductButton";
import AddProductButtonSmall from "../../common/buttons/AddProductButtonSmall";

const HeaderSearchAndAddToInventory = () => {
  const [showSearch, setShowSearch] = useState(false);

  return (
    <div className="header__search-addToInventory">
      <Search showSearch={showSearch} setShowSearch={setShowSearch} />
      <div
        className={`buttons__wrapper ${
          showSearch ? " buttons__wrapper--hide" : ""
        } ${
          JSON.parse(localStorage.getItem("user")).data.data.isAdmin
            ? " buttons__wrapper--admin"
            : ""
        }`}
      >
        <AddToCartButton />
        <AddProductButton />
      </div>
      <AddToCartButtonSmall showSearch={showSearch} />
      <AddProductButtonSmall showSearch={showSearch} />
      <BurgerMenu />
      <HelpIcon />
    </div>
  );
};

export default HeaderSearchAndAddToInventory;
