import "./Main.css";
import Catalog from "../catalog/Catalog";

const Main = () => {
  return (
    <main className="main">
      <Catalog />
    </main>
  );
};

export default Main;
